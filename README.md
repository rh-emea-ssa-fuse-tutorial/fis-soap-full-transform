# Full SOAP Transformations with CXF

This sample project is presented as a recommended approach on how to perform full SOAP-to-SOAP transformations when using CXF.

The need covered is to expose a SOAP service, perform data transformation, and then invoke a different back-end SOAP service, as the illustration below shows:

<p align="center">
<img src="images/soap-service.png" alt="drawing" width="300px"/>
</p>

The SOAP Envelope includes a SOAP Header part and SOAP Body. It would be ideal to execute the transformation in one step. This is best done using XSLTs (XML transformers).

<p align="center">
<img src="images/s1-to-s2.png" alt="drawing" width="500px"/>
</p>

Apache CXF is a Java Framework to create services. For (java programmatic) convenience CXF splits headers and body in separate entities. By creating some helper Camel routes both headers and body can be regrouped so that an XSLT StyleSheet can be applied to transform the data, as shown in the illustration below:

<p align="center">
<img src="images/camel-processing.png" alt="drawing" width="500px"/>
</p>

The picture below illustrates the entire end-to-end request and response flow pipelines of the presented approach:

<p align="center">
<img src="images/pipelines.png" alt="drawing" width="500px"/>
</p>


### Build and Run

This is a Fuse SpringBoot based project. The source code includes a simulated backend service.

To build and run it, execute the very simple Maven command:

    mvn

The project is configured with the default SpringBoot plugin goal 'spring-boot:run'. The command above compiles and launches the engine.

Once running you can use a test client such as SoapUI, for example, create a new project and give it the name  's1' with the initial WSDL address (exposed by SpringBoot):

    http://localhost:8080/services/s1?wsdl

SoapUI will create a default request that you can pre-populate and trigger. Fuse will process the request and the stub service will respond. SoapUI will receive the response when the end-to-end operation completes and you will obtain something similar to the picture below: 

<p align="center">
<img src="images/soapui-test.png" alt="drawing" width="500px"/>
</p>

