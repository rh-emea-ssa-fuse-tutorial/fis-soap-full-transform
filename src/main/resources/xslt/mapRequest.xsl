<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output indent="yes"/>

	<!-- Parameter IN containing SOAP Headers -->
	<xsl:param name="soapHeaders"/>

	<!-- Transformation definition -->
	<xsl:template match="/">

		<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:s2="http://www.example.org/s2/">
		   <soapenv:Header>
		      <s2:S2Header>
		         <h1><xsl:value-of select="$soapHeaders//header1"/></h1>
		         <h2><xsl:value-of select="$soapHeaders//header2"/></h2>
		      </s2:S2Header>
		   </soapenv:Header>
		   <soapenv:Body>
		      <s2:S2Request>
		         <in1><xsl:value-of select="//input1"/></in1>
		         <in2><xsl:value-of select="//input2"/></in2>
		      </s2:S2Request>
		   </soapenv:Body>
		</soapenv:Envelope>

	</xsl:template>
</xsl:stylesheet>