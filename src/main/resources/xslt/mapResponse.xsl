<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output indent="yes"/>
	
	<!-- Parameter IN containing SOAP Headers -->
	<xsl:param name="soapHeaders"/>

	<!-- Transformation definition -->
	<xsl:template match="/">

		<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:s1="http://www.example.org/s1/">
		   <soapenv:Header>
		      <s1:S1Header>
		         <header1><xsl:value-of select="$soapHeaders//h1"/></header1>
		         <header2><xsl:value-of select="$soapHeaders//h2"/></header2>
		      </s1:S1Header>
		   </soapenv:Header>
		   <soapenv:Body>
		      <s1:S1Response>
		         <output1><xsl:value-of select="//out1"/></output1>
		         <output2><xsl:value-of select="//out2"/></output2>
		      </s1:S1Response>
		   </soapenv:Body>
		</soapenv:Envelope>

	</xsl:template>
</xsl:stylesheet>